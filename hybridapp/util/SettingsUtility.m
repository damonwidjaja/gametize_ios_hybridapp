//
//  SettingsUtility.m
//  hybridapp
//
//  Created by Damon Widjaja on 21/5/18.
//  Copyright © 2018 Gametize Pte Ltd. All rights reserved.
//

#import "SettingsUtility.h"

static SettingsUtility *staticInstance = NULL;

@implementation SettingsUtility

@synthesize settingsDict;

- (id)init
{
    self = [super init];
    if (self) {
        [self readFromSettings];
    }
    return self;
}

#pragma mark - Singleton Boilerplate

+ (id)shared
{
    @synchronized(self)
    {
        if(staticInstance == NULL)
        {
            staticInstance = [[self alloc] init];
        }
    }
    
    return staticInstance;
}

#pragma mark - Read from plist

- (void) readFromSettings
{
    NSString *settingsFilePath = [[NSBundle mainBundle] pathForResource:SETTINGS_FILE_NAME ofType:@"plist"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:settingsFilePath]) {
        self.settingsDict = [NSDictionary dictionaryWithContentsOfFile:settingsFilePath];
    } else {
        [NSException raise:@"Missing Settings File" format:@"Settings File is missing"];
    }
}

@end
