//
//  SettingsUtility.h
//  hybridapp
//
//  Created by Damon Widjaja on 21/5/18.
//  Copyright © 2018 Gametize Pte Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SETTINGS_FILE_NAME @"settings"
#define GAMETIZE_URL @"gametize_url"
#define FIREBASE_ENABLED @"firebase_enabled"

@interface SettingsUtility : NSObject

@property (strong, nonatomic) NSDictionary *settingsDict;

#pragma mark - Singleton

+ (id)shared;

#pragma mark - Read from plist

- (void) readFromSettings;

@end
