//
//  ViewController.h
//  hybridapp
//
//  Created by Damon Widjaja on 15/5/18.
//  Copyright © 2018 Gametize Pte Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>
#import <Firebase/Firebase.h>

@interface ViewController : UIViewController <WKNavigationDelegate, WKUIDelegate> {
    BOOL isFirstTimeLoad;
    BOOL isFirebaseEnabled;
}

@property (strong, nonatomic) WKWebView *containerWebView;
@property (strong, nonatomic) UIView* loadingView;

- (void)initialiseWebView;
- (void)initialiseLoadingView;
- (void)trackPageView:(NSString *)pageView;
- (NSInteger)getStatusBarHeight:(float)screenHeight;

@end

