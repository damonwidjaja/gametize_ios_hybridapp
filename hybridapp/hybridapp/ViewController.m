//
//  ViewController.m
//  hybridapp
//
//  Created by Damon Widjaja on 15/5/18.
//  Copyright © 2018 Gametize Pte Ltd. All rights reserved.
//

#import "ViewController.h"
#import "SettingsUtility.h"

@interface ViewController ()

@end

@implementation ViewController

#pragma mark - Core Functions

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Set default variable
    isFirstTimeLoad = YES;
    
    //Retrieve Firebase enabled flag
    isFirebaseEnabled = [[[[SettingsUtility shared] settingsDict] objectForKey:FIREBASE_ENABLED] boolValue];
    
    //Initialise web view
    [self initialiseWebView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initialiseWebView {
    if(!_containerWebView) {
        //Retrieve device screen bounds
        CGRect screenBounds = [[UIScreen mainScreen] bounds];
        
        //Create a URL object
        NSURL *url = [NSURL URLWithString:[[[SettingsUtility shared] settingsDict] objectForKey:GAMETIZE_URL]];
        
        //URL Request Object
        NSMutableURLRequest *requestObj = [NSMutableURLRequest requestWithURL:url];
        
        //Inject script to enable scales page to fit and disable zooming
        NSString *jScript = @"var meta = document.createElement('meta'); \
        meta.name = 'viewport'; \
        meta.content = 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'; \
        var head = document.getElementsByTagName('head')[0];\
        head.appendChild(meta);";
        
        WKUserScript *wkUScript = [[WKUserScript alloc] initWithSource:jScript injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
        WKUserContentController *wkUController = [[WKUserContentController alloc] init];
        [wkUController addUserScript:wkUScript];
        
        WKWebViewConfiguration *wkWebConfig = [[WKWebViewConfiguration alloc] init];
        wkWebConfig.userContentController = wkUController;
        
        //Initialise WKWebView
        //To position WKWebView right below the status bar; therfore the need to retrieve status bar height
        _containerWebView = [[WKWebView alloc] initWithFrame:CGRectMake(0, [self getStatusBarHeight:screenBounds.size.height], screenBounds.size.width, screenBounds.size.height - [self getStatusBarHeight:screenBounds.size.height]) configuration:wkWebConfig];
        _containerWebView.navigationDelegate = self;
        _containerWebView.UIDelegate = self;
        
        //Disable WKWebView scrollview bounce
        _containerWebView.scrollView.bounces = NO;
        
        //Disable WKWebView scroll view content inset adjustment (Due to iPhone X safe area unintended padding)
        if (@available(iOS 11.0, *)) {
            _containerWebView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
        } else {
            //Do nothing
        }
        
        //Add webview to main view
        [self.view addSubview:_containerWebView];
        
        //Load the request in WKWebView
        [_containerWebView loadRequest:requestObj];
    }
}

#pragma mark - WKWebView Delegate

- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
    //To ensure loading view is only applied on initial web loading
    if(isFirstTimeLoad) {
        //Show loading view
        [self initialiseLoadingView];
        
        //Flag first time loaded as NO
        isFirstTimeLoad = NO;
    }
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    //If loading view is on super view, remove
    if([self.loadingView superview]) {
        [self.loadingView removeFromSuperview];
        self.loadingView = nil;
    }
    
    //Track page view
    [self trackPageView:[webView.URL absoluteString]];
}

- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    NSURL *targetUrl = navigationAction.request.URL;
    
    //Detect URL targetting out of main frame
    if (!navigationAction.targetFrame.isMainFrame) {
        WKFrameInfo *frameInfo = navigationAction.targetFrame;
        
        if(frameInfo) {
            //1. If frame information is available, assume target frame is meant for iFrame
            decisionHandler(WKNavigationActionPolicyAllow);
            return;
        }
        else {
            //2. If frame information is null, assume target frame is meant for _blank target
            
            //Open URL in Safari
            if([[UIApplication sharedApplication] canOpenURL:targetUrl]) {
                [[UIApplication sharedApplication] openURL:targetUrl];
                decisionHandler(WKNavigationActionPolicyCancel);
                return;
            }
        }
    }
    
    //Detect URL targetting email and telephone app
    if([targetUrl.scheme isEqualToString:@"mailto"] || [targetUrl.scheme isEqualToString:@"tel"]) {
        //Open email or telephone app
        [[UIApplication sharedApplication] openURL:targetUrl];
        decisionHandler(WKNavigationActionPolicyCancel);
        return;
    }
    
    decisionHandler(WKNavigationActionPolicyAllow);
    return;
}

#pragma mark - View Functions

- (NSInteger)getStatusBarHeight:(float)screenHeight {
    //Determine status bar height
    if(screenHeight >= 812) //The height of iPhone X and [Probably future phone]
        return 44;
    else
        return 20;
}

- (void)initialiseLoadingView {
    if(!self.loadingView) {
        //Construct loading view
        self.loadingView = [[UIView alloc] initWithFrame:CGRectMake(_containerWebView.frame.origin.x, _containerWebView.frame.origin.y, _containerWebView.frame.size.width, _containerWebView.frame.size.height)];
        [self.loadingView setBackgroundColor:[UIColor whiteColor]];
        
        //Construct activity indicator view
        UIActivityIndicatorView* activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicatorView.hidesWhenStopped = YES;
        [activityIndicatorView startAnimating];
        activityIndicatorView.center = CGPointMake(self.loadingView.frame.size.width/2, self.loadingView.frame.size.height/2);
        
        [self.loadingView addSubview:activityIndicatorView];
    }
    
    //If loading view is not on super view, display
    if(![self.loadingView superview]) {
        [self.view addSubview:self.loadingView];
    }
}

#pragma mark - Firebase Functions

- (void)trackPageView:(NSString *)pageUrl
{
    //Track Firebase event
    if(isFirebaseEnabled) {
        [FIRAnalytics logEventWithName:kFIREventSelectContent
                            parameters:@{
                                         kFIRParameterItemID:[NSString stringWithFormat:@"id-%@", pageUrl],
                                         kFIRParameterItemName:pageUrl,
                                         kFIRParameterContentType:@"page-url"
                                         }];
    }
}

@end
