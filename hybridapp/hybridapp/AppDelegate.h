//
//  AppDelegate.h
//  hybridapp
//
//  Created by Damon Widjaja on 15/5/18.
//  Copyright © 2018 Gametize Pte Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Firebase/Firebase.h>
#import <UserNotifications/UserNotifications.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate, FIRMessagingDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

